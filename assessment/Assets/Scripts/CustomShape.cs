﻿using System;
using System.Collections.Generic;
using System.Linq;
using Common;
using UnityEngine;
using UnityEngine.UI;

public class CustomShape : MonoBehaviour
{
    public Dropdown ColorInput;
    public Dropdown ShapeInput;
    public InputField WidthInput, HeightInput, DepthInput;

    public void Start()
    {
        SetupColorDropdown();
        SetupShapeDropdown();
    }

    private void SetupColorDropdown()
    {
        List<Colors> colorsList = Enum.GetValues(typeof(Colors)).Cast<Colors>().ToList();
        List<Dropdown.OptionData> list = new List<Dropdown.OptionData>();
        foreach (Colors color in colorsList)
            list.Add(new Dropdown.OptionData(color.ToString()));
        if(ColorInput != null)
            ColorInput.options = list;
    }
    
    private void SetupShapeDropdown()
    {
        List<Shapes> shapesList = Enum.GetValues(typeof(Shapes)).Cast<Shapes>().ToList();
        List<Dropdown.OptionData> list = new List<Dropdown.OptionData>();
        foreach (Shapes shape in shapesList)
            list.Add(new Dropdown.OptionData(shape.ToString()));
        if(ShapeInput != null)
            ShapeInput.options = list;
    }

    public void AddCustom()
    {
        PrimitiveObject po = ScriptableObject.CreateInstance<PrimitiveObject>();
        po.SetValues($"{ColorInput.value.ToString()},{ShapeInput.value.ToString()}," +
                     $"{GetInputValue(WidthInput)},{GetInputValue(HeightInput)},{GetInputValue(DepthInput)}");
        
        PrimitiveCollection.CreateObject.Invoke(po);
    }

    private float GetInputValue(InputField input)
    {
        try
        {
            float value = Convert.ToSingle(input.text);
            return value;
        }
        catch (Exception e)
        {
            return 1;
        }
    }
    
}
