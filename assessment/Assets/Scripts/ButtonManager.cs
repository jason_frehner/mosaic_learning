using System;
using Unity.Profiling.LowLevel.Unsafe;
using UnityEngine;
using UnityEngine.UI;


public class ButtonManager : MonoBehaviour
{
    public GameObject ButtonPrefab;

    public void Start()
    {
        if (ButtonPrefab == null)
            ButtonPrefab = Resources.Load<GameObject>("Prefabs/ButtonPreFab");
        PrimitiveCollection.AddedToCollection.AddListener(AddButton);
        AddButton("Clear", ClearObjectsEvent);
    }

    public void Reset()
    {
        ClearButtons();
        PrimitiveCollection.AddedToCollection.RemoveListener(AddButton);
    }

    private void ClearButtons()
    {
        int count = transform.childCount;
        for (int i = count - 1; i >= 0; i--)
        {
            GameObject go = transform.GetChild(i).gameObject;
            go.SetActive(false);
            go.transform.SetParent(null);
            DestroyImmediate(go);
        }
        transform.DetachChildren();
    }

    private void AddButton(PrimitiveObject primitiveObject)
    {
        GameObject bf = Instantiate(ButtonPrefab, transform, true);
        bf.name = primitiveObject.Shape.ToString();
        bf.transform.GetChild(0).GetComponent<Text>().text = primitiveObject.Color.ToString();
        ColorUtility.TryParseHtmlString(primitiveObject.Color.ToString(), out Color color);
        bf.transform.GetChild(0).GetComponent<Text>().color = color;
        bf.GetComponent<Button>().onClick.AddListener(() =>
        {
            CreateObjectEvent(primitiveObject);
        });
    }

    private void AddButton(string text, Action method)
    {
        GameObject bf = Instantiate(ButtonPrefab, transform, true);
        bf.name = text;
        bf.transform.GetChild(0).GetComponent<Text>().text = text;
        bf.GetComponent<Button>().onClick.AddListener(() =>
        {
            method();
        });
    }

    private void CreateObjectEvent(PrimitiveObject primitiveObject)
    {
        PrimitiveCollection.CreateObject.Invoke(primitiveObject);
    }

    private void ClearObjectsEvent()
    {
        GameObject[] primitiveInSceneList = GameObject.FindGameObjectsWithTag("Primitive");
        foreach (GameObject o in primitiveInSceneList)
        {
            //DestroyImmediate(o);
            o.SetActive(false);
        }
    }
}