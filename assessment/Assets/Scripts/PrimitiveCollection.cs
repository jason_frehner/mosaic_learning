﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using UnityEngine;
using UnityEngine.Events;

public class PrimitiveCollection : MonoBehaviour
{
    public enum Sources
    {
         Local, Web
    }
    
    public List<PrimitiveObject> ObjectCollection;
    public Shader Shader;
    public Sources Source;

    private List<GameObject> objectPool = new List<GameObject>();
    
    public static UnityEvent<PrimitiveObject> AddedToCollection = new UnityEvent<PrimitiveObject>();
    public static UnityEvent<PrimitiveObject> CreateObject = new UnityEvent<PrimitiveObject>();

    public void Start()
    {
        if (Shader == null)
            Shader = Resources.Load<Shader>("SimpleShader");

        if (Source == Sources.Local)
            SetupObjectCollectionFromScriptableObjects();
        else
            SetupObjectCollectonFromWebRequest();
        
        CreateObject.AddListener(InstantiateObject);
    }

    private void SetupObjectCollectonFromWebRequest()
    {
        ObjectCollection = new List<PrimitiveObject>();
        string request = GetCSV();
        List<string> lines = request.Trim().Split('\n').ToList();
        lines.RemoveAt(0);
        foreach (string line in lines)
        {
            PrimitiveObject po = ScriptableObject.CreateInstance<PrimitiveObject>();
            po.SetValues(line);
            AddObjectToCollection(po);
        }
    }

    private string GetCSV()
    {
        WebRequest request = WebRequest.Create("https://unity-developer-assessment.s3.amazonaws.com/objects.csv");
        HttpWebResponse webResponse = (HttpWebResponse) request.GetResponse();
        Stream stream = webResponse.GetResponseStream();
        StreamReader reader = new StreamReader(stream);
        string response = reader.ReadToEnd();
        return response;
    }

    private void SetupObjectCollectionFromScriptableObjects()
    {
        ObjectCollection = new List<PrimitiveObject>();
        AddObjectToCollection("RedCube");
        AddObjectToCollection("BlueSphere");
        AddObjectToCollection("YellowCone");
    }

    private void AddObjectToCollection(string scriptableObject)
    {
        PrimitiveObject o = Resources.Load<PrimitiveObject>($"Scriptable Objects/{scriptableObject}");
        AddObjectToCollection(o);
    }
    
    private void AddObjectToCollection(PrimitiveObject primitiveObject)
    {
        ObjectCollection.Add(primitiveObject);
        AddedToCollection.Invoke(primitiveObject);
    }
    
    private void InstantiateObject(PrimitiveObject primitiveObject)
    {
        GameObject poolObject = CheckForPoolObject(primitiveObject);
        if(poolObject)
            poolObject.SetActive(true);
        else
        {
            GameObject go = Instantiate(Resources.Load<GameObject>($"Prefabs/{primitiveObject.Shape}"));
            go.transform.GetChild(0).GetComponent<MeshRenderer>().material = new Material(Shader);
            ColorUtility.TryParseHtmlString(primitiveObject.Color.ToString(), out Color color);
            go.transform.GetChild(0).GetComponent<Renderer>().sharedMaterial.color = color;
            go.transform.localScale = new Vector3(primitiveObject.Width, primitiveObject.Height, primitiveObject.Depth);
            objectPool.Add(go);
        }
    }

    private GameObject CheckForPoolObject(PrimitiveObject primitiveObject)
    {
        ColorUtility.TryParseHtmlString(primitiveObject.Color.ToString(), out Color color);
        
        List<GameObject> poolOfShape = objectPool.Where(x => 
            x.name.Contains(primitiveObject.Shape.ToString()) &&
            x.transform.GetChild(0).GetComponent<Renderer>().sharedMaterial.color == color &&
            Math.Abs(x.transform.localScale.x - primitiveObject.Width) < 0.1f &&
            Math.Abs(x.transform.localScale.y - primitiveObject.Height) < 0.1f &&
            Math.Abs(x.transform.localScale.z - primitiveObject.Depth) < 0.1f).ToList();
        
        GameObject poolObject = null;
        if (poolOfShape.Count > 0)
            poolObject = poolOfShape.FirstOrDefault(x => !x.activeSelf);
        return poolObject;
    }
}
