using UnityEngine;
//Code from https://www.patreon.com/posts/unity-3d-drag-22917454

public class DragObject : MonoBehaviour
{
    private Vector3 moveOffset;
    private float zCoord;

    void OnMouseDown()
    {
        Vector3 position = gameObject.transform.position;
        if (!(Camera.main is null)) zCoord = Camera.main.WorldToScreenPoint(position).z;
        moveOffset = position - GetMouseAsWorldPoint();
    }

    private Vector3 GetMouseAsWorldPoint()
    {
        Vector3 mousePoint = Input.mousePosition;
        mousePoint.z = zCoord;
        return !(Camera.main is null) ? Camera.main.ScreenToWorldPoint(mousePoint) : default;
    }

    void OnMouseDrag()
    {
        transform.position = GetMouseAsWorldPoint() + moveOffset;
    }
}