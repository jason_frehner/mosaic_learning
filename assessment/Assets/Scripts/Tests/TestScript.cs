﻿using System.Linq;
using Common;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.UI;

namespace Tests
{
    public class ScriptableObjectsTests
    {
        [Test]
        public void RedCubeValues()
        {
            PrimitiveObject o = Resources.Load<PrimitiveObject>("Scriptable Objects/RedCube");
            Assert.NotNull(o);
            Assert.AreEqual(Colors.Red, o.Color);
            Assert.AreEqual(Shapes.Cube, o.Shape);
            Assert.AreEqual(1, o.Width);
            Assert.AreEqual(1, o.Height);
            Assert.AreEqual(1, o.Depth);
        }

        [Test]
        public void BlueSphereValues()
        {
            PrimitiveObject o = Resources.Load<PrimitiveObject>("Scriptable Objects/BlueSphere");
            Assert.NotNull(o);
            Assert.AreEqual(Colors.Blue, o.Color);
            Assert.AreEqual(Shapes.Sphere, o.Shape);
            Assert.AreEqual(2, o.Width);
            Assert.AreEqual(0.5, o.Height);
            Assert.AreEqual(2, o.Depth);
        }

        [Test]
        public void YellowConeValues()
        {
            PrimitiveObject o = Resources.Load<PrimitiveObject>("Scriptable Objects/YellowCone");
            Assert.NotNull(o);
            Assert.AreEqual(Colors.Yellow, o.Color);
            Assert.AreEqual(Shapes.Cone, o.Shape);
            Assert.AreEqual(0.5, o.Width);
            Assert.AreEqual(2, o.Height);
            Assert.AreEqual(0.5, o.Depth);
        }
    }

    public class CollectionTests
    {
        private GameObject go;
        private ButtonManager bm;
        private PrimitiveCollection pc;
        
        [OneTimeSetUp]
        public void Setup()
        {
            go = new GameObject();
            bm = go.AddComponent<ButtonManager>();
            bm.Start();

            pc = go.AddComponent<PrimitiveCollection>();
            pc.Source = PrimitiveCollection.Sources.Local;
            pc.Start();
        }

        [Test]
        public void CheckCollection()
        {
            Assert.NotNull(pc.ObjectCollection);
            Assert.AreEqual(3, pc.ObjectCollection.Count);
            CheckPrimitiveObjectIsInTheCollection("RedCube");
            CheckPrimitiveObjectIsInTheCollection("BlueSphere");
            CheckPrimitiveObjectIsInTheCollection("YellowCone");
        }

        [Test]
        public void CheckButtonsSetup()
        {
            Assert.AreEqual(4, bm.transform.childCount);
            CheckButtonNames("Clear", bm.transform.GetChild(0));
            CheckButtonNames("Red", bm.transform.GetChild(1));
            CheckButtonNames("Blue", bm.transform.GetChild(2));
            CheckButtonNames("Yellow", bm.transform.GetChild(3));
        }

        [Test]
        public void CheckButtonsInteractions()
        {
            Assert.AreEqual(0, GameObject.FindGameObjectsWithTag("Primitive").Length);
            CheckButton(bm.transform.GetChild(1).GetComponent<Button>(), "Cube");
            CheckButton(bm.transform.GetChild(2).GetComponent<Button>(), "Sphere");
            CheckButton(bm.transform.GetChild(3).GetComponent<Button>(), "Cone");
            Assert.AreEqual(3, GameObject.FindGameObjectsWithTag("Primitive").Length);
            bm.transform.GetChild(0).GetComponent<Button>().onClick.Invoke();
            Assert.AreEqual(0, GameObject.FindGameObjectsWithTag("Primitive").Length);
        }

        private void CheckButton(Button button, string name)
        {
            button.onClick.Invoke();
            GameObject[] primitiveInSceneList = GameObject.FindGameObjectsWithTag("Primitive");
            Assert.IsTrue(primitiveInSceneList.Any(x => x.ToString().Contains(name)));
        }

        private void CheckPrimitiveObjectIsInTheCollection(string primitiveObjectName)
        {
            Assert.IsTrue(
                pc.ObjectCollection.Contains(
                    Resources.Load<PrimitiveObject>($"Scriptable Objects/{primitiveObjectName}")));
        }

        private void CheckButtonNames(string name, Transform button)
        {
            Assert.AreEqual(name, button.GetChild(0).GetComponent<Text>().text);
        }

        [Test]
        public void WebRequestTest()
        {
            bm.Reset();
            bm.Start();
            pc.Source = PrimitiveCollection.Sources.Web;
            pc.Start();
            CheckButtonsSetup();
        }
    }

    public class CustomAdd
    {
        [Test]
        public void PopulateDropdowns()
        {
            GameObject go = new GameObject();
            CustomShape cs = go.AddComponent<CustomShape>();
            cs.Start();
        }
    }
}
