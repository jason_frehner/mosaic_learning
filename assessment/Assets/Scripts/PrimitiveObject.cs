﻿using System;
using Common;
using UnityEngine;

[CreateAssetMenu(fileName = "Object", menuName = "CustomObject/Object", order = 1)]
public class PrimitiveObject : ScriptableObject
{
    public Colors Color;
    public Shapes Shape;
    public float Width, Height, Depth;

    public void SetValues(string values)
    {
        string[] v = values.Split(',');
        Colors.TryParse(v[0], out Color);
        Shapes.TryParse(v[1], out Shape);
        Width = Convert.ToSingle(v[2]);
        Height = Convert.ToSingle(v[3]);
        Depth = Convert.ToSingle(v[4]);
    }
}